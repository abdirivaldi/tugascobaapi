import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {BASE_URL, TOKEN} from './url';
import Icon from 'react-native-vector-icons/AntDesign';

const AddData = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [textInputNama, settextInputNama] = useState('');
  const [textInputHarga, settextInputHarga] = useState('');
  const [textInputKM, settextInputKM] = useState('');
  const [isTextInputEmpty, setIsTextInputEmpty] = useState(false);
  var dataMobil = route.params;
  //Untuk postdata dari sini
  const postData = async () => {
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      // navigation.goBack();
      console.log('Success:', result);
    } catch (error) {
      console.error('Error:', error);
    }
  };
  //sampai sini post data
  useEffect(() => {
    console.log('data sebelumnya', route.params);
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  //editdata dari sini
  const editData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      alert('Produk berhasil ditambahkan');
    } catch (error) {
      console.error('Error:', error);
    }
  };
  //sampai sini edit data

  //deletedata darisini
  const deleteData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      alert('Data Mobil berhasil dihapus');
      navigation.navigate('Home');
    } catch (error) {
      console.error('Error:', error);
    }
  };
  //sampaisini

  const checkTextInput = () => {
    if (!namaMobil || !totalKM || !hargaMobil) {
      alert('Masih ada yang kosong tuh Bwang !!');
      return false;
    } else {
      return true;
    }
  };
  const handlePressNext = () => {
    if (checkTextInput()) {
      Alert.alert('Mantap', 'Data Mobil berhasil ditambahkan');
      postData();
      navigation.navigate('Home');
    } else {
    }
  };

  // const handlePressNext = () => {
  //   if (!namaMobil) {
  //     return alert('nama mobil kosong');
  //   }
  //   if (!totalKM) {
  //     return alert('total mobil kosong');
  //   }
  //   if (!hargaMobil) {
  //     return alert('harga mobil kosong');
  //   }

  //   // if (isValid) {
  //   //   Alert.alert('Mantap', 'Data Mobil berhasil ditambahkan');
  //   //   postData();
  //   //   navigation.navigate('Home');
  //   // } else {
  //   //   alert('Masih ada yang kosong tuh Bwang !!');
  //   // }
  // };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      {isTextInputEmpty && (
        <Text style={styles.error}>Isi semua dulu doong</Text>
      )}
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Detail')}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="arrowleft" size={20} color="#000" />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          {dataMobil ? 'Ubah Data' : 'TambahData'}
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            value={namaMobil}
            placeholder="Masukkan Nama Mobil"
            style={styles.txtInput}
            onChangeText={text => setNamaMobil(text)}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            placeholder="contoh: 100 KM"
            style={styles.txtInput}
            onChangeText={text => setTotalKM(text)}
            value={totalKM}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Harga Mobil"
            style={styles.txtInput}
            value={hargaMobil}
            keyboardType="number-pad"
            onChangeText={text => setHargaMobil(text)}
          />
        </View>
        <TouchableOpacity style={styles.btnAdd}>
          <Text
            style={{color: '#fff', fontWeight: '600'}}
            onPress={() => handlePressNext()}>
            Tambah Data
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
