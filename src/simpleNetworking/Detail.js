import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Modal,
  Pressable,
  Dimensions,
  TextInput,
} from 'react-native';
import {BASE_URL, TOKEN} from './url';

const Detail = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [modalVisible, setModalVisible] = useState('');
  const [imageURL, setImageURL] = useState('');
  const [textInputNama, settextInputNama] = useState('');
  const [textInputHarga, settextInputHarga] = useState('');
  const [textInputKM, settextInputKM] = useState('');
  const [isTextInputEmpty, setIsTextInputEmpty] = useState(false);
  var dataMobil = route.params;
  useEffect(() => {
    console.log('data sebelumnya', route.params);
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
      setImageURL(data.unitImage);
    }
  }, []);
  const editData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      navigation.navigate('Home');
      setImageURL(body[0].unitImage);
    } catch (error) {
      console.error('Error:', error);
    }
  };
  //sampai sini edit data

  //deletedata darisini
  const deleteData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      alert('Data Mobil berhasil dihapus');
      navigation.navigate('Home');
    } catch (error) {
      console.error('Error:', error);
    }
  };
  const checkTextInput = () => {
    if (!namaMobil || !totalKM || !hargaMobil) {
      alert('Masih ada yang kosong tuh Bwang !!');
      return false;
    } else {
      return true;
    }
  };
  const handlePressNext = () => {
    if (checkTextInput()) {
      Alert.alert('Mantap', 'Cie cie berhasil diganti');
      editData();
      navigation.navigate('Home');
    } else {
    }
  };
  return (
    <View style={styles.container}>
      <Image
        style={{
          height: 316,
          width: Dimensions.get('window').width,
          width: '100%',
        }}
        source={
          imageURL
            ? {uri: imageURL}
            : require('../assets/image/Gambarilang.png')
        }
      />

      <View style={styles.kotak}>
        <TouchableOpacity onPress={() => setModalVisible(true)}>
          <View style={styles.kotakedit}>
            <Text style={{fontSize: 40, color: 'white'}}>EDIT DATA</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => deleteData()}>
          <View style={styles.kotakdelete}>
            <Text style={{fontSize: 40, color: 'white'}}>DELETE DATA</Text>
          </View>
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
            navigation.navigate('Home');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.latarmodal}>
              <View style={{alignItems: 'center', marginBottom: 5}}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#000',
                    fontWeight: '600',
                    marginLeft: 5,
                  }}>
                  Nama Mobil
                </Text>
                <TextInput
                  value={namaMobil}
                  placeholder="Masukkan Nama Mobil"
                  style={styles.txtInput}
                  onChangeText={text => setNamaMobil(text)}
                />
              </View>
              <View style={{alignItems: 'center', marginBottom: 5}}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#000',
                    fontWeight: '600',
                    marginLeft: 5,
                  }}>
                  Total Kilometer
                </Text>
                <TextInput
                  placeholder="contoh: 100 KM"
                  style={styles.txtInput}
                  onChangeText={text => setTotalKM(text)}
                  value={totalKM}
                />
              </View>
              <View style={{alignItems: 'center', marginBottom: 5}}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#000',
                    fontWeight: '600',
                    marginLeft: 5,
                  }}>
                  Harga Mobil
                </Text>
                <TextInput
                  placeholder="Masukkan Harga Mobil"
                  style={styles.txtInput}
                  value={hargaMobil}
                  keyboardType="number-pad"
                  onChangeText={text => setHargaMobil(text)}
                />
                <TouchableOpacity onPress={() => handlePressNext()}>
                  <View style={styles.kotakeditmodal}>
                    <Text style={{fontSize: 40, color: 'white'}}>
                      Edit Data
                    </Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.kotakcancelmodal}>
                  <TouchableOpacity
                    onPress={() => setModalVisible(!modalVisible)}>
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: 'bold',
                        color: 'white',
                      }}>
                      CANCEL
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
};
export default Detail;

const styles = StyleSheet.create({
  container: {flex: 1},
  kotak: {
    flex: 1,
    width: '100%',
    height: '100%',
    marginTop: 20,
    elevation: 3,
    backgroundColor: 'white',
    paddingTop: 10,
    borderRadius: 20,
    marginTop: -25,
    borderWidth: 2,
    borderColor: 'aqua',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  kotaktambah: {
    backgroundColor: 'green',
    borderRadius: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 200,
    height: 100,
    paddingVertical: 20,
    marginVertical: 10,
  },
  kotakedit: {
    backgroundColor: 'blue',
    borderRadius: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 200,
    height: 100,
    paddingVertical: 20,
    marginVertical: 10,
  },
  kotakdelete: {
    backgroundColor: 'red',
    borderRadius: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 200,
    height: 100,
    paddingVertical: 20,
    marginVertical: 10,
  },
  kotakeditmodal: {
    backgroundColor: 'green',
    borderRadius: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 200,
    height: 100,
    paddingVertical: 20,
    marginVertical: 10,
  },
  kotakcancelmodal: {
    backgroundColor: 'grey',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
    alignItems: 'center',
  },
  latarmodal: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
    backgroundColor: 'white',
  },
  centeredView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 0, 0, 0.1)',
    alignContent: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: 250,
    borderRadius: 6,
    borderColor: '#dedede',
    borderWidth: 1,
    borderColor: 'black',
    paddingHorizontal: 10,
  },
});
